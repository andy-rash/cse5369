# Homework 2: Counterfeits, PUFs, and Anti-Tampering

Robert Rash  
CSE 5369  
6 December 2018

1. **The survey we studied from the U.S. Department of Commerce listed several types of counterfeit parts. What were they?**

	* Electromechanical
	* Thyristors
	* Capacitors
	* Other discrete electronic components
	* Circuit protection/fuses
	* Diodes
	* Resistors
	* Sensors and actuators
	* Optoelectronics
	* Power transistors
	* Rectifiers
	* Small signal transistors
	* Magnetics
	* Crystals/oscillators
	* Microprocessors
	* Other microcircuits
	* Memory
	* Radio frequency/wireless
	* Logic (standard & special purpose)

2. **Of the counterfeit types you listed in part (1), what type was most common for microcircuits? What type was most common for discrete electronic components? Why do you think these might be different?**

	**Microcircuits (i.e. integrated circuits--ICs)**: Microprocessors & other microcircuits; counterfeits were mostly "used product re-marked as higher grade new product"
	
	**Discrete electronic components (DECs)**: Electromechanical & Thyristors; counterfeits were mostly "fake, non-working products"

	These might be different because, for one matter, these are fairly distinct categories both in terms of their manufacturing processes as well as their end use cases. For DECs, these are almost always simpler structurally, so counterfeiters have an easier time simply selling junk, non-working components. Whereas for ICs, they are much more complicated "under the hood", so it's easier for counterfeiters to simply purchase cheaper, used products and re-mark them.

3. **Explain why counterfeit electronics have the potential to be especially problematic in military applications.**

	Military applications require much more extreme tolerances than can be had with commercial-grade items. It's a difficult problem to source MILSPEC components under the best of circumstances. Because counterfeit electronics suffer from poor reliability and performance, among other things, they pose a great risk when used under circumstances that require such stringent specs.

4. **What is a broker, and what is the relationship between brokers and counterfeits?**

	Brokers are sellers of parts acquired from various, possibly unreputable, sources and who do so without an exclusive OCM/OEM agreement. Usually, these are small firms that do not keep inventory and have less controlled environments.

	Brokers, as a category, see much higher incidence of counterfeits since fewer brokers adhere to industry standards when sourcing components.

5. **Physically Unclonable Functions (PUFs) have been suggested as a good choice for key generation and chip IDs. What is a PUF and what characteristics should a good PUF possess?**

	A physically unclonable function (PUF) is a unique function that maps a set of challenges to a set of responses that are determined by certain intractable physical characteristics (e.g. measurements of statistical variation in gate delays).

	Characteristics of a good PUF:
	
	* repeated measurements/output values for a single PUF challenge on the same physical chip should remain the same, even in the presence of environmental changes (temp., aging, voltage changes, etc.)
	* Applying the same challenge to two of the same PUFs on two different chips should yield statistically independent results

6. **One type of PUF that we studied is an arbiter PUF. Draw a schematic for an arbiter PUF that with a 3-bit challenge and explain how the PUF should work to generate a 1-bit response.**

(See appendix for schematic)

Arbiter PUFs are symmetric, so ideally, the inputs will travel from one side of the circuit to the other simultaneously, switching sides based on the provided 3-bit challenge. However, due to inevitable process variations, one of the two inputs will arrive at the terminating flip flop before the other, with the flip flop capturing either a `0` or a `1`. This is the 1-bit output.

7. **Another type of PUF that we studied was a PUF based on Ring Oscillators. The figure below [see Appendix] shows part of a Ring Oscillator based PUF.**

	a. **What additional hardware is needed to make a PUF from these ROs?**
	
	Because ROs are susceptible to environmental changes (i.e. they are not good PUFs on their own), it is necessary to add hardware that uses their output in a consistent way, rather than relying on the ROs themselves to be consistent. One example of this is attaching two pairs of ROs to a mux (2 ROs for each mux), attaching the output of each of these muxes to two counters, and then after a certain period of time, determining which counter has the larger value, which will output either a `0` or a `1`.
	
	b. **What is the frequency of each RO, assuming that the delay is determined by the delays shown on each gate?**
	
	* EN1: 19.92 GHz
	* EN2: 19.57 GHz
	* EN3: 20.04 GHz
	* EN4: 19.61 GHz
	
	c. **Assume that from these 4 ROs you want to get a consistent 2-bit response. Which ROs should be compared to generate each bit? Justify your answer.**
	
	To obtain consistent results, we must choose ROs that have frequencies that are farther apart. Additionally, since we are wanting a 2-bit response, we must use 2 ROs per bit. Choosing from the frequencies above, the pairs that are furthest apart are as follows:
	
	* `EN1` (19.92 GHz) and `EN2` (19.57 GHz); delta: 0.35 GHz
	* `EN3` (20.04 GHz) and `EN4` (19.61 GHz); delta: 0.43 GHz
	
	d. **How far apart will the counts be if you allot 1 millisecond for taking each measurement?**
	
	* _Counter 1:_ 2.857 ns
	* _Counter 2:_ 2.326 ns
	
	e. **What is the minimum number of bits your counters could possibly have to obtain this data?**
	
	2 bits each
	
8. **Two other PUFs we studied were implemented in FPGAs. Although their implementation was different, they were both designed to take advantage of a fundamental characteristic of each circuit. Describe these two PUFs and explain how their operation is related.**

	* Butterfly PUF
		* Relies on the instability created by two cross-coupled latches as well as their process variations to determine the output. 
	* FPGA LUT PUF
		* Uses lookup-tables (LUTs) as shift registers. With a carry chain connecting each LUT, process variations can cause "glitches" that result in pseudorandom `0`s or `1`s.

	Like the other PUFs that we have learned about, these PUFs ultimately rely on process variations to determine the output values. With these PUFs, however, the design utilizes structures that are already in place in FPGAs, which makes them more widely available than some other PUF implementations.

9. **Explain the difference between tamper resistant, tamper responding, and tamper evident protection. Give at least one example of each.**

	* tamper resistant
		* 	resistance implies steps taken to prevent tampering in the first place (e.g. proprietary screws on iPhone severely limits unauthorized access to phone internals)
	* tamper responding
		* "punishes" the attacker for tamper attempts (e.g. microprocessor zeros the memory upon tamper detection)
	* tamper evident
		* provides proof of tampering, but does little to stop concerted attempts (e.g. foil lining on unopened medicine bottles)

10. **One of the common responses to detected tampering of a physically secure electronic device is the erasure of the memory.**

	a. **What kind of memory configuration lends itself to easy erasure?**
	
	battery-backed RAM
	
	b. **What types of techniques are used by attackers to try to imprint the memory so that data can still be obtained by the attacker?**
	
	One method is by physically freezing the memory. This will cause the memory cells to retain their values without a power source.
	
	c. **What sensors may be used to try to prevent imprinting?**

	To counteract the above example, temperature sensors may be used to detect the temperature and overwrite the memory before the chip ever reaches freezing temperatures.

## Appendix

**3-bit in/1-bit out Arbiter PUF**

![3-bit in/1-bit out Arbiter PUF](img/CSE5369-Arbiter-PUF.png)

**Ring Oscillator based PUF**

![Ring Oscillator based PUF](img/ring-oscillator-puf.png)
